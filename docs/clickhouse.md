CREATE TABLE audit (
id UUID, 
type String,
action String, 
entity String NULL, 
entity_id UInt32 NULL,
description String NULL, 
field String NULL, 
old_value String NULL, 
new_value String NULL, 
user_id UInt32, 
event_date Date,
event_time DateTime
)
ENGINE MergeTree()
ORDER BY (id, event_time)
PRIMARY KEY id
PARTITION BY toYYYYMM(event_date)
