package service

import (
	"audit/internal/domain"
	"audit/internal/repository"
	"context"
)

type Audit interface {
	CreateAudit(ctx context.Context, input []domain.CreateAuditInput) (int, error)
	GetList(ctx context.Context, requestDto domain.GetAuditRequestDto) (domain.GetAuditResponseDto, error)
}

type Service struct {
	Audit Audit
}

func NewServices(repos *repository.Repositories) *Service {
	return &Service{
		Audit: NewAuditService(repos.Audit),
	}
}
