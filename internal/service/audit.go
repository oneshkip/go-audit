package service

import (
	"audit/internal/domain"
	"audit/internal/repository"
	"context"
)

type AuditService struct {
	repo repository.Audit
}

func NewAuditService(audit repository.Audit) *AuditService {
	return &AuditService{repo: audit}
}

func (a *AuditService) CreateAudit(ctx context.Context, input []domain.CreateAuditInput) (int, error) {
	return a.repo.Create(ctx, input)
}

func (a *AuditService) GetList(ctx context.Context, requestDto domain.GetAuditRequestDto) (domain.GetAuditResponseDto, error) {
	var responseDto domain.GetAuditResponseDto
	var meta domain.AuditResponseMeta

	dbResult, err := a.repo.Get(requestDto)

	meta.Page = requestDto.Page
	meta.PerPage = requestDto.PerPage

	responseDto.Meta = meta
	responseDto.Data = dbResult

	return responseDto, err

}
