package repository

import (
	"audit/internal/domain"
	"audit/internal/repository/chdb"
	"context"
	"database/sql"
)

type Audit interface {
	Create(ctx context.Context, input []domain.CreateAuditInput) (int, error)
	Get(dto domain.GetAuditRequestDto) ([]*domain.Audit, error)
}

type Repositories struct {
	Audit Audit
}

func NewRepositories(db *sql.DB) *Repositories {
	return &Repositories{
		Audit: chdb.NewAuditRepository(db),
	}
}
