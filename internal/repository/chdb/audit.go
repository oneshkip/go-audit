package chdb

import (
	"audit/internal/domain"
	"context"
	"database/sql"
	"log"
)

type AuditRepository struct {
	db *sql.DB
}

func NewAuditRepository(db *sql.DB) *AuditRepository {
	return &AuditRepository{db: db}
}

func (r AuditRepository) Create(ctx context.Context, input []domain.CreateAuditInput) (int, error) {
	var (
		tx, _   = r.db.Begin()
		stmt, _ = tx.Prepare("INSERT INTO audit SELECT generateUUIDv4(),?,?,?,?,?,?,?,?,?,today(),now()")
	)
	defer tx.Rollback()

	defer stmt.Close()

	for _, inp := range input {

		if _, err := stmt.Exec(
			inp.Type,
			inp.Action,
			inp.Entity,
			inp.EntityId,
			inp.Description,
			inp.Field,
			inp.OldValue,
			inp.NewValue,
			inp.UserId,
		); err != nil {
			log.Fatal(err)
		}
	}

	err := tx.Commit()

	return 1, err
}

func (r AuditRepository) Get(request domain.GetAuditRequestDto) ([]*domain.Audit, error) {

	var query string
	query = "SELECT id, type, entity, entity_id, description, field, old_value, new_value, user_id, event_time FROM audit ORDER BY event_time DESC LIMIT ?"

	var (
		tx, _     = r.db.Begin()
		rows, err = tx.Query(query, request.PerPage)
	)
	defer rows.Close()

	results := make([]*domain.Audit, 0)

	for rows.Next() {
		audit := new(domain.Audit)
		err := rows.Scan(&audit.ID, &audit.Type, &audit.Entity, &audit.EntityId, &audit.Description, &audit.Field, &audit.OldValue, &audit.NewValue, &audit.UserId, &audit.EventTime)

		if err != nil {
			log.Fatal(err)
		}

		results = append(results, audit)
	}

	return results, err
}
