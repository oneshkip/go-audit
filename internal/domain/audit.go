package domain

import "database/sql"

type Audit struct {
	ID          string        `json:"id"`
	Type        string        `json:"type"`
	Entity      string        `json:"entity"`
	EntityId    sql.NullInt32 `json:"entity_id"`
	Field       string        `json:"field"`
	Description string        `json:"description"`
	OldValue    string        `json:"old_value"`
	NewValue    string        `json:"new_value"`
	UserId      int           `json:"user_id"`
	EventTime   string        `json:"event_time"`
}

type CreateAuditInput struct {
	Type        string `json:"type"`
	Action      string `json:"action"`
	Entity      string `json:"entity"`
	EntityId    int32  `json:"entity_id"`
	Description string `json:"description"`
	Field       string `json:"field"`
	OldValue    string `json:"old_value"`
	NewValue    string `json:"new_value"`
	UserId      int32  `json:"user_id"`
}

type AuditCollection []CreateAuditInput

type GetAuditRequestDto struct {
	Page    int
	PerPage int
	User    int
	Filters map[string]string
}

type GetAuditResponseDto struct {
	Data []*Audit          `json:"data"`
	Meta AuditResponseMeta `json:"meta"`
}

type AuditResponseMeta struct {
	Page     int `json:"page"`
	PerPage  int `json:"per_page"`
	LastPage int `json:"last_page"`
	Total    int `json:"total"`
}
