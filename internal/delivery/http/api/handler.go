package api

import (
	"audit/internal/service"
	"github.com/gorilla/mux"
)

type Handler struct {
	services *service.Service
}

func NewHandler(serv *service.Service) *Handler {
	return &Handler{services: serv}
}

func (h *Handler) Init() *mux.Router  {
	router := mux.NewRouter()
	h.initAPI(router)
	return router
}

func (h *Handler) initAPI(router *mux.Router)  {
	h.InitAuditRoutes(router)
}