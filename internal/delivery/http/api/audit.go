package api

import (
	"audit/internal/domain"
	"encoding/json"
	"github.com/gorilla/mux"
	"io/ioutil"
	"log"
	"net/http"
	"strconv"
)

func (h *Handler) InitAuditRoutes(router *mux.Router) {
	router.HandleFunc("/api/audit", h.CreateAudit()).Methods("POST")
	router.HandleFunc("/api/audit", h.GetAuditList()).Methods("GET")
}

func (h *Handler) CreateAudit() http.HandlerFunc {
	return func(writer http.ResponseWriter, request *http.Request) {
		var auditInput []domain.CreateAuditInput

		body, readErr := ioutil.ReadAll(request.Body)

		if readErr != nil {
			http.Error(writer, readErr.Error(), http.StatusBadRequest)
			return
		}
		err := json.Unmarshal(body, &auditInput)

		if err != nil {
			http.Error(writer, err.Error(), http.StatusBadRequest)
			return
		}

		res, r := h.services.Audit.CreateAudit(request.Context(), auditInput)

		if res != 1 {
			return
		}

		if r != nil {
			log.Fatal(r)
		}

		resp := json.NewEncoder(writer).Encode(auditInput)

		if resp == nil {
			return
		}
	}
}

func (h *Handler) GetAuditList() http.HandlerFunc {
	return func(writer http.ResponseWriter, request *http.Request) {
		var requestDto domain.GetAuditRequestDto

		pageParam := request.URL.Query().Get("page")

		if pageParam != "" {
			pageParamInt, err := strconv.Atoi(pageParam)
			if err == nil {
				requestDto.Page = pageParamInt
			}
		}

		perPageParam := request.URL.Query().Get("per_page")

		if perPageParam != "" {
			perPageParamInt, err := strconv.Atoi(perPageParam)
			if err == nil {
				requestDto.PerPage = perPageParamInt
			}
		} else {
			requestDto.PerPage = 20
		}

		userParam := request.URL.Query().Get("user")

		if pageParam != "" {
			userParamInt, err := strconv.Atoi(userParam)
			if err == nil {
				requestDto.User = userParamInt
			}
		}
		respDto, err := h.services.Audit.GetList(request.Context(), requestDto)

		if err != nil {
			log.Fatal(err)
			return
		}

		resp := json.NewEncoder(writer).Encode(respDto)

		if resp == nil {
			return
		}
	}
}

func createFilters() {

}
