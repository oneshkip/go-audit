package middleware

import (
	"log"
	"mime"
	"net/http"
)

func CheckJsonHeaderMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(writer http.ResponseWriter, request *http.Request) {
		contentType := request.Header.Get("Content-Type")

		if contentType == "" {
			http.Error(writer, "Content-Type not set", http.StatusBadRequest)
			return
		}

		mt, _, err := mime.ParseMediaType(contentType)
		if err != nil {
			http.Error(writer, "Malformed Content-Type header", http.StatusBadRequest)
			return
		}

		if mt != "application/json" {
			http.Error(writer, "Content-Type header must be application/json", http.StatusUnsupportedMediaType)
			return
		}

		next.ServeHTTP(writer, request)
	})
}

func RequestLoggerMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(writer http.ResponseWriter, request *http.Request) {
		uri := request.RequestURI
		ip := request.RemoteAddr

		log.Printf("Uri: %v, IP: %v, Context: %v\n", uri, ip, request.Context().Value("someKey"))

		next.ServeHTTP(writer, request)
	})
}

func AddJsonContentHeaderToResponse(next http.Handler) http.Handler {
	return http.HandlerFunc(func(writer http.ResponseWriter, request *http.Request) {
		writer.Header().Add("Content-Type", "application/json")

		next.ServeHTTP(writer, request)
	})
}
