package app

import (
	"audit/internal/delivery/http/api"
	"audit/internal/delivery/http/middleware"
	"audit/internal/repository"
	"audit/internal/service"
	"audit/pkg/database/clickhouse"
	"log"
	"net/http"
	"os"
)

func Run() {
	log.Println("Starting application")

	client := clickhouse.NewClient()
	repos := repository.NewRepositories(client)
	services := service.NewServices(repos)
	handler := api.NewHandler(services)

	a := handler.Init()
	a.Use(
		middleware.RequestLoggerMiddleware,
		middleware.CheckJsonHeaderMiddleware,
		middleware.AddJsonContentHeaderToResponse,
	)

	http.Handle("/", a)

	err := http.ListenAndServe(":8000", nil)

	check(err)
}

func check(e error) {
	if e != nil {
		log.Println(e)
		os.Exit(1)
	}
}
