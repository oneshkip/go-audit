package clickhouse

import (
	"database/sql"
	"github.com/ClickHouse/clickhouse-go"
	"log"
)

func NewClient() *sql.DB {
	client, err := sql.Open("clickhouse", "tcp://clickhouse:9000?debug=false")

	if err != nil {
		log.Fatalf("Error occured while establishing connect to clickhouse")
	}

	if err := client.Ping(); err != nil {
		if exception, ok := err.(*clickhouse.Exception); ok {
			log.Fatalf("[%d] %s \n%s\n", exception.Code, exception.Message, exception.StackTrace)
		} else {
			log.Fatal(err)
		}
		log.Fatal(err)
	}

	return client
}
