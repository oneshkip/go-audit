module audit

go 1.16

require (
	github.com/ClickHouse/clickhouse-go v1.4.5
	github.com/gorilla/mux v1.8.0
)
