FROM golang:1.16.4-buster
RUN mkdir -p /app
WORKDIR /app
ADD . /app

RUN go build /app/cmd/app/main.go

EXPOSE 8000

CMD ["/app/main"]

